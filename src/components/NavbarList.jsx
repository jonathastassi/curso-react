import React from 'react'

class NavbarList extends React.Component {
    constructor(props) {
        super(props)

        this.state = { menuAtivo: "Home"};
        this.alterarActive = this.alterarActive.bind(this);
    }

    alterarActive(titulo, self) {
        self.setState({menuAtivo: titulo});
    }
    
    render() {
        
        let self = this;

        let menus = this.props.items.map(function(item, index) {
            return <li key={index} onClick={self.alterarActive.bind(null, item.titulo, self)} className={self.state.menuAtivo === item.titulo ? 'active' : ''}><a href={item.link}>{item.titulo}</a></li>
        })

        return (
            <ul className="right">
                {menus}
            </ul>
        );
    }
}

export default NavbarList