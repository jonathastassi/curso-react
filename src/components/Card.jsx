import React from 'react'

class Card extends React.Component {

    render() {
        // console.log(this.props.list)    
        return (
            <div className="card sticky-action">
                <div className="card-image waves-effect waves-block waves-light" onClick={this.props.list.incClick}>
                    <img className="activator" src={this.props.data.imagem} alt={this.props.data.titulo}/>
                </div>
                <div className="card-content">
                    <span className="card-title activator grey-text text-darken-4">{this.props.data.titulo}<i className="material-icons right">more_vert</i></span>
                    <p>{this.props.data.descricao}</p>
                </div>
                <div className="card-action">
                    <a href={this.props.data.link}>Clique aqui</a>
                </div>
                <div className="card-reveal">
                    <span className="card-title grey-text text-darken-4">{this.props.data.titulo}<i className="material-icons right">close</i></span>
                    <p>{this.props.data.detalhe}</p>
                </div>
            </div>
        )
    }
}

export default Card