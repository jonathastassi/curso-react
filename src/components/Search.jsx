import React from 'react'

class Search extends React.Component {
    constructor(props) {
        super(props);

        this.searchBusca = this.searchBusca.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    searchBusca(event) {
        this.props.searchBusca(event)
    }

    onSubmit(event) {
        this.props.onSubmit(event);
    }

    render() {
        return (
            <form onSubmit={this.onSubmit} className="col s12">
                <div className="input-field col s6">
                    <input id="icon_prefix" type="text" onChange={this.searchBusca} value={this.props.busca} className="validate" />
                    <label htmlFor="icon_prefix">Pesquisar</label>
                </div>
                <button type="submit" className="btn">Pesquisar</button>
            </form>
        );
    }    
}

export default Search