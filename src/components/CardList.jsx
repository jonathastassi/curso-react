import React from 'react'
import axios from 'axios'

import Card from './Card'
import Search from './Search'

class CardList extends React.Component {
    constructor(props) {
        super(props);

        this.state = { qtdeClick: 0, busca: '', dados: [], servidor: []};
        this.incClick = this.incClick.bind(this);
        this.searchBusca = this.searchBusca.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    incClick() {
        // let qtde = this.state.qtdeClick;
        // qtde++;
        
        this.setState((prevState) => ({
            qtdeClick: prevState.qtdeClick + 1
        }));
    }

    searchBusca(event) {
        this.setState({busca: event.target.value});

        if (event.target.value === "") {
            this.setState({dados: this.state.servidor})
        }
    }

    componentDidMount() {

        let self = this;

        axios.get('http://192.168.1.39/servidor.php?dados=0')
            .then(function (response) {
                self.setState({dados: response.data, servidor: response.data});
            })
    }

    onSubmit(event) {

        let busca = this.state.busca;
        let dados = this.state.servidor;

        let novaLista = dados.filter(function(item) {
            if(item.titulo.toUpperCase().indexOf(busca.toUpperCase()) > -1) {
                return item;
            }
            return ""
        });

        this.setState({dados: novaLista});
        event.preventDefault();
    }

    render() {

        let noticias = this.state.dados

        let col = []
        let lines = [];

        for(let j = 0; j < noticias.length; j++) {
            if(col.length === this.props.qtdeLine) {
                lines.push(col);
                col = [];
            }
            col.push(noticias[j])        
        }
        lines.push(col);
            
        
        let getColumns = (line) => {
            let classColuna = "col m"+this.props.sizeCard;

            let self = this;

            return line.map(function(col, index) {
                return <div  key={index} className={classColuna}>
                            <Card data={col} list={self}/>
                        </div>
            })
        }

        let linejsx = lines.map(function(line, index) {
            return <div  key={index} className="row">
                        {getColumns(line)}
                   </div>
        })


        return (
            // <div className="row">
            <div>
                <p>{this.state.qtdeClick}</p>
                <Search searchBusca={this.searchBusca} busca={this.state.busca} onSubmit={this.onSubmit}/>  {this.state.busca}
                {linejsx}
            </div>
            // </div>
        )
    }
}

export default CardList