import React from 'react'

class Title extends React.Component {
    constructor(props) {
        super(props)

        this.state = {hora: new Date()}
    }

    componentDidMount() {
        this.hora_id = setInterval(() => this.alteraHorario(),1000);
    }

    alteraHorario() {
        this.setState({hora: new Date()})
    }

    componentWillMount() {
        clearInterval(this.hora_id)
    }

    render() {

        let self = this;

        return (
            <React.Fragment>
                <h1 style={{marginBottom: 0}}>{this.props.text}</h1>
                <small style={{fontSize: 24}}>{this.props.small}</small>
                <p>{self.state.hora.toLocaleTimeString()}</p>
            </React.Fragment>
        )
    }
}

export default Title