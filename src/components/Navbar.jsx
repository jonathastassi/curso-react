import React from 'react'
import NavbarList from './NavbarList'
import axios from 'axios'

class Navbar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {menu: []}
    }

    componentDidMount() {
        let self = this;
        axios.get('http://192.168.1.39/servidor.php?menu=0').then((response) => {
            self.setState({menu: response.data});
        })
    }

    render() {
        let items = this.state.menu;

        return (<div className="navbar-fixed">
                    <nav>
                        <div className="container">
                            <div className="nav-wrapper">
                                <a href="#!" className="brand-logo">{this.props.title}</a>
                                <NavbarList items={items} />
                            </div>
                        </div>
                    </nav>
                </div>);
    }
}

export default Navbar 