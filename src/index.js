import React from 'react'
import ReactDom from 'react-dom'

import Navbar from './components/Navbar'
import Title from './components/Title'
import CardList from './components/CardList'

let html = <React.Fragment>
                <Navbar title="Curso React" color="blue" />
                <div className="container">
                    <Title text="Curso de React" small="Jonathas Tassi e Silva" />
                    <CardList sizeCard="4" qtdeLine="3" />
                    <div className="clearfix"></div>
                </div>

           </React.Fragment>

ReactDom.render(html, document.getElementById('root'))
